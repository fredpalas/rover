<?php


namespace App\Model;


class Obstacles
{
    /**
     * @var bool
     */
    private $random;
    /**
     * @var int
     */
    private $min;
    /**
     * @var int
     */
    private $max;
    /**
     * @var int|null
     */
    private $obstaclesNumber;
    /**
     * @var array|null
     */
    private $obstacles;
    /**
     * @var array
     */
    private $obstaclesPosition = [];

    /**
     * Obstacles constructor.
     * @param bool $random
     * @param int $width
     * @param int $height
     * @param int|null $obstaclesNumber
     * @param array|null $obstacles
     * @throws \Exception
     */
    public function __construct(bool $random, int $width, int $height, ?int $obstaclesNumber = null, ?array $obstacles = null) {

        if (!$random &&  (
            count($obstacles) != $obstaclesNumber
            || !($numeric = $this->checkArray($obstacles))
            || ($b = (count($obstacles) !== count(array_unique($obstacles))))
            || !($a = $this->isArrayBetween($obstacles, 0, $width * $height))
        )) {
            $message = 'Obstacles array is not valid,' .
                (count($obstacles) != $obstaclesNumber
                    ? sprintf(' Obstacles number %s different at count %s in array', $obstaclesNumber, count($obstacles))
                    : '') .
                ((isset($numeric) && !$numeric )? ' Obstacles array is not a range of int' : '' ).
                ((isset($b) && $b) ? ' Obstacles array have repetitive values' : '' ).
                ((isset($a) && !$a) ? sprintf(' Obstacle not in the range between %s and %s', 0, $width * $height) : '')
            ;

            throw new \Exception($message);
        }
        if ($random) {
            $size = ($width * $height) - 1;
            $obstaclesPosition = range(1, $size + 1);
            shuffle($obstaclesPosition);
            $this->obstaclesPosition = array_slice($obstaclesPosition, 0, $obstaclesNumber);
        }

        $this->random = $random;
        $this->min = $width;
        $this->max = $height;
        $this->obstaclesNumber = $obstaclesNumber;
        $this->obstacles = $obstacles;
    }

    private function checkArray($array)
    {
        foreach ($array as $a) {
            if (!is_int(intval($a))) {
                return false;
            }
        }
        return true;
    }

    public function getObstaclesArray(): array
    {
        if (!$this->random) {
            return $this->obstacles;
        }

        return $this->obstaclesPosition;
    }

    private function isArrayBetween($array, $min, $max): bool
    {
        foreach ($array as $a) {
            if (intval($a) < $min || intval($a) > $max) {
                return false;
            }
        }
        return true;
    }
}