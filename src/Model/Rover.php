<?php


namespace App\Model;


class Rover
{

    const NOT_ALLOWED_MOVEMENT = 0;
    const OBSTACLE_DETECTED = 1;

    const FACING_NORTH = 'N';
    const FACING_SOUTH = 'S';
    const FACING_EST = 'E';
    const FACING_WEST = 'W';
    /**
     * @var Planet
     */
    private $planet;
    /**
     * @var int
     */
    private $xPosition;
    /**
     * @var int
     */
    private $yPosition;
    /**
     * @var string
     */
    private $facing;

    /** @var null | int */
    private $lastXPosition = null;
    /** @var null | int */
    private $lastYPosition = null;
    /**
     * @var string
     */
    private $lastFacing;

    /**
     * Rover constructor.
     * @param Planet $planet
     * @param int $xPosition
     * @param int $yPosition
     * @param string $facing
     * @throws \Exception
     */
    public function __construct(Planet $planet, int $xPosition, int $yPosition, string $facing) {

        if (!in_array($facing, [self::FACING_NORTH, self::FACING_SOUTH, self::FACING_EST, self::FACING_WEST])) {
            throw new \Exception(sprintf('Facing %s not valid', $facing));
        }

        if ($xPosition < 0 && $xPosition > $planet->getWidth() - 1) {
            throw new \Exception(sprintf('X Position %s not valid range between %s and %s', $facing, 0, $planet->getWidth()));
        }

        if ($yPosition < 0 && $yPosition > $planet->getHeight() - 1) {
            throw new \Exception(sprintf('Y Position %s not valid range between %s and %s', $facing, 0, $planet->getHeight()));
        }

        $this->planet = $planet;
        $this->lastXPosition = $this->xPosition = $xPosition;
        $this->lastYPosition = $this->yPosition = $yPosition;
        $this->facing = $facing;
    }

    /**
     * @return Planet
     */
    public function getPlanet(): Planet
    {
        return $this->planet;
    }

    /**
     * @return int
     */
    public function getXPosition(): int
    {
        return $this->xPosition;
    }

    /**
     * @return int
     */
    public function getYPosition(): int
    {
        return $this->yPosition;
    }

    /**
     * @return string
     */
    public function getFacing(): string
    {
        return $this->facing;
    }

    /**
     * @return $this
     * @throws \Exception
     */
    public function moveForward(): self
    {
        $this->setLastPosition();
        switch ($this->facing) {
            case self::FACING_NORTH:
                $this->moveNorth();
                break;
            case self::FACING_SOUTH:
                $this->moveSouth();
                break;
            case self::FACING_WEST:
                $this->moveWest();
                break;
            case self::FACING_EST:
                $this->moveEst();
                break;
        }

        if ($this->checkObstacle()) {
            $x = $this->xPosition;
            $y = $this->yPosition;
            $this->setPositionToLast();
            throw new \Exception(sprintf('Obstacle detected moving Forward on %s X, %s Y', $x, $y), self::OBSTACLE_DETECTED);
        }

        return $this;
    }

    /**
     * @return $this
     * @throws \Exception
     */
    public function moveRight(): self
    {
        $this->setLastPosition();
        switch ($this->facing) {
            case self::FACING_NORTH:
                $this->moveEst();
                break;
            case self::FACING_SOUTH:
                $this->moveWest();
                break;
            case self::FACING_WEST:
                $this->moveNorth();
                break;
            case self::FACING_EST:
                $this->moveSouth();
                break;
        }

        if ($this->checkObstacle()) {
            $x = $this->xPosition;
            $y = $this->yPosition;
            $this->setPositionToLast();
            throw new \Exception(sprintf('Obstacle detected moving Right on %s X, %s Y', $x, $y), self::OBSTACLE_DETECTED);
        }

        return $this;
    }

    /**
     * @return $this
     * @throws \Exception
     */
    public function moveLeft(): self
    {
        $this->setLastPosition();
        switch ($this->facing) {
            case self::FACING_NORTH:
                $this->moveWest();
                break;
            case self::FACING_SOUTH:
                $this->moveEst();
                break;
            case self::FACING_WEST:
                $this->moveSouth();
                break;
            case self::FACING_EST:
                $this->moveNorth();
                break;
        }

        if ($this->checkObstacle()) {
            $x = $this->xPosition;
            $y = $this->yPosition;
            $this->setPositionToLast();
            throw new \Exception(sprintf('Obstacle detected moving Left on %s X, %s Y', $x, $y), self::OBSTACLE_DETECTED);
        }

        return $this;
    }

    private function moveNorth()
    {
        if ($this->yPosition == $this->planet->getHeight() - 1) {
            throw new \Exception('Facing North and Y position is 0 can not move Forward', self::NOT_ALLOWED_MOVEMENT);
        }
        $this->facing = self::FACING_NORTH;
        $this->yPosition += 1;
    }

    private function moveSouth()
    {
        if ($this->yPosition == 0) {
            throw new \Exception(sprintf('Facing South and Y position is %s can not move Forward', $this->yPosition), self::NOT_ALLOWED_MOVEMENT);
        }
        $this->facing = self::FACING_SOUTH;
        $this->yPosition -= 1;
    }

    private function moveEst()
    {
        if ($this->xPosition == $this->planet->getWidth() - 1) {
            throw new \Exception(sprintf('Facing EST and X position is %s can not move Forward', $this->xPosition), self::NOT_ALLOWED_MOVEMENT);
        }
        $this->facing = self::FACING_EST;
        $this->xPosition += 1;
    }

    private function moveWest()
    {
        if ($this->xPosition == 0) {
            throw new \Exception('Facing West and X position is 0 can not move Forward', self::NOT_ALLOWED_MOVEMENT);
        }
        $this->facing = self::FACING_WEST;
        $this->xPosition -= 1;
    }

    private function setLastPosition()
    {
        $this->lastXPosition = $this->xPosition;
        $this->lastYPosition = $this->yPosition;
        $this->lastFacing = $this->facing;
    }

    private function setPositionToLast()
    {
        $this->xPosition = $this->lastXPosition ;
        $this->yPosition = $this->lastYPosition;
        $this->facing = $this->lastFacing;
    }

    private function checkObstacle()
    {
        $cell = ($this->xPosition) + ($this->yPosition * 10);
        $obstaclesPosition = $this->planet->getObstaclesPosition();

        return in_array($cell + 1, $obstaclesPosition);
    }
}