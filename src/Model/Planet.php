<?php


namespace App\Model;


class Planet
{
    /** @var int  */
    private $height;
    /** @var int  */
    private $width;
    /** @var int  */
    private $obstacles;
    /** @var array  */
    private $map;

    private $obstaclesPosition;

    public function __construct(int $height, int $width, Obstacles $obstacles) {

        $this->height = $height;
        $this->width = $width;
        $this->obstacles = $obstacles;

        $this->map = array_map("array_fill_keys", array_fill(0, $height, array("id", "value")), range(0, $width - 1));
        $this->obstaclesPosition = $obstacles->getObstaclesArray();
    }

    /**
     * @return int
     */
    public function getHeight(): int
    {
        return $this->height;
    }

    /**
     * @return int
     */
    public function getWidth(): int
    {
        return $this->width;
    }

    /**
     * @return int
     */
    public function getObstacles(): int
    {
        return $this->obstacles;
    }

    /**
     * @return array
     */
    public function getMap(): array
    {
        return $this->map;
    }

    /**
     * @return array
     */
    public function getObstaclesPosition(): array
    {
        return $this->obstaclesPosition;
    }


}