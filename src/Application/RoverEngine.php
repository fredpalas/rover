<?php


namespace App\Application;


use App\Exceptions\RoverException;
use App\Model\Obstacles;
use App\Model\Planet;
use App\Model\Rover;

class RoverEngine
{

    const MOVEMENTS = [
        'FORWARD' => 0,
        'RIGHT' => 1,
        'LEFT' => 2,
    ];

    /**
     * @var Planet
     */
    private $planet;
    /**
     * @var Rover
     */
    private $rover;

    /**
     * RoverEngine constructor.
     * @param int $height
     * @param int $width
     * @param int $obstacles
     * @param int $xPosition
     * @param int $yPosition
     * @param string $facing
     * @param array|null $obstaclesArray
     * @throws RoverException
     * @throws \Exception
     */
    public function __construct(
        int $height,
        int $width,
        int $obstacles,
        int $xPosition,
        int $yPosition,
        string $facing,
        ?array $obstaclesArray = null
    ) {
        $obstaclesObject = new Obstacles(!$obstaclesArray, $width, $height, $obstacles, $obstaclesArray);
        $this->planet = new Planet($height, $width, $obstaclesObject);
        $cell = ($xPosition + 1) * ($yPosition + 1);
        $obstaclesPosition = $this->planet->getObstaclesPosition();
        $this->rover = new Rover($this->planet, $xPosition, $yPosition, $facing);
        if (in_array($cell, $obstaclesPosition)) {
            throw new RoverException($this->rover, sprintf('%s X and %s Y position exist a obstacle', $xPosition, $yPosition), 0);
        }
    }


    /**
     * @param int $movement
     * @return Rover
     * @throws RoverException
     */
    public function moveTo(int $movement)
    {
        try {
            switch ($movement) {
                case (self::MOVEMENTS['FORWARD']):
                    $this->rover->moveForward();
                    break;
                case (self::MOVEMENTS['RIGHT']):
                    $this->rover->moveRight();
                    break;
                case (self::MOVEMENTS['LEFT']):
                    $this->rover->moveLeft();
                    break;
            }
        } catch (\Exception $exception) {
            throw new RoverException($this->rover, $exception->getMessage(), $exception->getCode());
        }


        return $this->rover;
    }

    /**
     * @return Rover
     */
    public function getRover(): Rover
    {
        return $this->rover;
    }
}