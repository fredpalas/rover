<?php

namespace App\Command;

use App\Application\RoverEngine;
use App\Exceptions\RoverException;
use App\Model\Rover;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class RoverCommand extends Command
{

    const MOVEMENTS = [
        'F' => RoverEngine::MOVEMENTS['FORWARD'],
        'L' => RoverEngine::MOVEMENTS['LEFT'],
        'R' => RoverEngine::MOVEMENTS['RIGHT']
    ];

    protected static $defaultName = 'app:rover';
    protected static $defaultDescription = 'Rover received command, grid 0,0 is south-west';

    protected function configure(): void
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addArgument('commands', InputArgument::REQUIRED, 'Choice x position')
            ->addOption('xPos','x', InputOption::VALUE_REQUIRED, 'Choice x position, default 0', 0)
            ->addOption('yPos','y', InputOption::VALUE_REQUIRED, 'Choice y position, default 0', 0)
            ->addOption('facing', 'f',InputOption::VALUE_REQUIRED, 'choice facing N, S, E, W, default N', 'N')
            ->addOption('height', 'H',InputOption::VALUE_REQUIRED, 'choice land height default 10', 10)
            ->addOption('width', 'w',InputOption::VALUE_REQUIRED, 'choice land width default 10', 10)
            ->addOption('obstacles', 'o', InputOption::VALUE_REQUIRED, 'choice number of obstacles default 20', 20)
            ->addOption('obstacles-position', 'O', InputOption::VALUE_IS_ARRAY | InputOption::VALUE_OPTIONAL, 'set where set obstacles on a range multiply width x height', null)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $positions = $input->getArgument('commands');
        $xPos = (int)$input->getOption('xPos') ?? 0;
        $yPos = (int)$input->getOption('yPos') ?? 0;
        $facing = $input->getOption('facing') ?? 'N';
        $height = $input->getOption('height') ?? 10;
        $width = $input->getOption('width') ?? 10;
        $obstacles = $input->getOption('obstacles') ?? 20;
        $obstaclesPosition = $input->getOption('obstacles-position');
        $obstaclesPosition = count($obstaclesPosition) > 0 ? $obstaclesPosition : null;

        try {
            $roverEngine = new RoverEngine($height, $width, $obstacles, $xPos, $yPos, $facing, $obstaclesPosition);
        } catch (\Exception $e) {
            $io->error(sprintf('Error: %s', $e->getMessage()));
            return 1;
        }

        $output->writeln(sprintf('Rover on %s X, %s Y facing %s', $xPos, $yPos, $facing));

        try {
            foreach (str_split($positions) as $position) {
                if (!in_array($position, array_keys(self::MOVEMENTS))) {
                    $io->error(sprintf('Error: instruction %s not valid', $position));
                    return 1;
                }
                $return = $roverEngine->moveTo(self::MOVEMENTS[$position]);

                $output->writeln(
                    sprintf(
                        'Rover moved to %s X, %s Y facing %s',
                        $return->getXPosition(),
                        $return->getYPosition(),
                        $return->getFacing()
                    )
                );
            }
        } catch (RoverException $e) {
            if ($e->getCode() === Rover::NOT_ALLOWED_MOVEMENT) {
                $io->error(sprintf('Error: %s', $e->getMessage()));
                return 1;
            }
            if ($e->getCode() === Rover::OBSTACLE_DETECTED) {
                $io->error(
                    sprintf(
                        'Error: %s moving back to %s X, %s Y facing %s',
                        $e->getMessage(),
                        $e->getRover()->getXPosition(),
                        $e->getRover()->getYPosition(),
                        $e->getRover()->getFacing()
                    )
                );
                return 1;
            }
        }



        $io->success('You have arrive to your destination');

        return 0;
    }
}
