<?php


namespace App\Exceptions;


use App\Model\Rover;
use Throwable;

class RoverException extends \Exception
{
    /**
     * @var Rover
     */
    private $rover;

    public function __construct(Rover $rover, $message = "", $code = 0) {
        parent::__construct($message, $code);
        $this->rover = $rover;
    }

    /**
     * @return Rover
     */
    public function getRover(): Rover
    {
        return $this->rover;
    }
}