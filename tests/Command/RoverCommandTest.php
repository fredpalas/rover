<?php

namespace App\Tests\Command;

use App\Command\RoverCommand;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class RoverCommandTest extends KernelTestCase
{
    /**
     * @var CommandTester
     */
    private $commandTester = null;

    public function setUp(): void
    {
        parent::setUp();
        if (null === $this->commandTester) {
            $kernel = static::createKernel();
            $application = new Application($kernel);
            $command = $application->find('app:rover');
            $this->commandTester = new CommandTester($command);
        }
    }

    public function testMovementValid()
    {
        $commandTester = $this->commandTester;
        $commandTester->execute([
            'commands' => 'FRLRFRLLFFL',
            '--facing' => 'N',
            '--xPos' => 0,
            '--yPos' => 0,
            '--height' => 10,
            '--width' => 10,
            '--obstacles' => 6,
            '--obstacles-position' => [46,39,32,64,48,4]
        ]);

        // the output of the command in the console
        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('Rover moved to 0 X, 1 Y facing N', $output);
        $this->assertStringContainsString('Rover moved to 3 X, 2 Y facing E', $output);
        $this->assertStringContainsString('Rover moved to 3 X, 1 Y facing S', $output);
        $this->assertStringContainsString('Rover moved to 3 X, 4 Y facing W', $output);
    }

    public function testMovementInvalid()
    {
        $commandTester = $this->commandTester;
        $commandTester->execute([
            'commands' => 'FRLRFRLLFFL',
            '--facing' => 'N',
            '--xPos' => 0,
            '--yPos' => 0,
            '--height' => 10,
            '--width' => 10,
            '--obstacles' => 6,
            '--obstacles-position' => [46,39,35,64,48,4]
        ]);

        // the output of the command in the console
        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('Rover moved to 0 X, 1 Y facing N', $output);
        $this->assertStringContainsString('Rover moved to 3 X, 2 Y facing E', $output);
        $this->assertStringContainsString('Rover moved to 3 X, 1 Y facing S', $output);
        $this->assertStringContainsString('Rover moved to 4 X, 2 Y facing N', $output);
        $this->assertStringContainsString('Error: Obstacle detected moving Forward on 4 X, 3 Y moving back to 4 X, 2 Y facing N', $output);
    }

    public function testMovementInvalidCommand()
    {
        $commandTester = $this->commandTester;
        $commandTester->execute([
            'commands' => 'FTRLRFRLLFFL',
            '--facing' => 'N',
            '--xPos' => 0,
            '--yPos' => 0,
            '--height' => 10,
            '--width' => 10,
            '--obstacles' => 6,
            '--obstacles-position' => [46,39,35,64,48,4]
        ]);

        // the output of the command in the console
        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('Rover moved to 0 X, 1 Y facing N', $output);
        $this->assertStringContainsString('Error: instruction T not valid', $output);
    }

    public function testMovementInvalidObstacleArrayNumber()
    {
        $commandTester = $this->commandTester;
        $commandTester->execute([
            'commands' => 'FRLRFRLLFFL',
            '--facing' => 'N',
            '--xPos' => 0,
            '--yPos' => 0,
            '--height' => 10,
            '--width' => 10,
            '--obstacles' => 6,
            '--obstacles-position' => [46,39,35,64,48,4, 11]
        ]);

        // the output of the command in the console
        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('[ERROR] Error: Obstacles array is not valid, Obstacles number 6 different at count 7 in array ', $output);
    }

    public function testMovementInvalidObstacleArrayRepetitive()
    {
        $commandTester = $this->commandTester;
        $commandTester->execute([
            'commands' => 'FRLRFRLLFFL',
            '--facing' => 'N',
            '--xPos' => 0,
            '--yPos' => 0,
            '--height' => 10,
            '--width' => 10,
            '--obstacles' => 6,
            '--obstacles-position' => [46,39,35,64,4,4]
        ]);

        // the output of the command in the console
        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('[ERROR] Error: Obstacles array is not valid, Obstacles array have repetitive values', $output);
    }

    public function testMovementInvalidObstacleArrayOutRange()
    {
        $commandTester = $this->commandTester;
        $commandTester->execute([
            'commands' => 'FRLRFRLLFFL',
            '--facing' => 'N',
            '--xPos' => 0,
            '--yPos' => 0,
            '--height' => 10,
            '--width' => 10,
            '--obstacles' => 6,
            '--obstacles-position' => [46,39,35,64,4,101]
        ]);

        // the output of the command in the console
        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('[ERROR] Error: Obstacles array is not valid, Obstacle not in the range between 0 and 100', $output);
    }
}
