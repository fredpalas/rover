# Rover Test

Application Made with Symfony

## Installation 

### Requirements

PHP 7.3 >=

[Composer](https://getcomposer.org/download/) needed for install the app

### Instruction 

```bash
composer install
```
## Test
Made With PhpUnit and [Symfony PHPUnit Bridge](https://symfony.com/doc/current/testing.html#the-phpunit-testing-framework)

```bash
bin/phpunit
```



## Grid Format

### Cardinal Points on the grid

North(N) Top <br>
South(S) Bottom <br>
Est(E) Right <br>
West(W) Left

#### GRID With X and Y coordinate and cell number 
<table>
<tbody>
<tr>
<th>9</th>
<td align="right" >91</td>
<td align="right" >92</td>
<td align="right" >93</td>
<td align="right" >94</td>
<td align="right" >95</td>
<td align="right" >96</td>
<td align="right" >97</td>
<td align="right" >98</td>
<td align="right" >99</td>
<td align="right" >100</td>
</tr>
<tr>
<th>8</th>
<td align="right" >81</td>
<td align="right" >82</td>
<td align="right" >83</td>
<td align="right" >84</td>
<td align="right" >85</td>
<td align="right" >86</td>
<td align="right" >87</td>
<td align="right" >88</td>
<td align="right" >89</td>
<td align="right" >90</td>
</tr>
<tr>
<th>7</th>
<td align="right" >71</td>
<td align="right" >72</td>
<td align="right" >73</td>
<td align="right" >74</td>
<td align="right" >75</td>
<td align="right" >76</td>
<td align="right" >77</td>
<td align="right" >78</td>
<td align="right" >79</td>
<td align="right" >80</td>
</tr>
<tr>
<th>6</th>
<td align="right" >61</td>
<td align="right" >62</td>
<td align="right" >63</td>
<td align="right" >64</td>
<td align="right" >65</td>
<td align="right" >66</td>
<td align="right" >67</td>
<td align="right" >68</td>
<td align="right" >69</td>
<td align="right" >70</td>
</tr>
<tr>
<th>5</th>
<td align="right" >51</td>
<td align="right" >52</td>
<td align="right" >53</td>
<td align="right" >54</td>
<td align="right" >55</td>
<td align="right" >56</td>
<td align="right" >57</td>
<td align="right" >58</td>
<td align="right" >59</td>
<td align="right" >60</td>
</tr>
<tr>
<th>4</th>
<td align="right" >41</td>
<td align="right" >42</td>
<td align="right" >43</td>
<td align="right" >44</td>
<td align="right" >45</td>
<td align="right" >46</td>
<td align="right" >47</td>
<td align="right" >48</td>
<td align="right" >49</td>
<td align="right" >50</td>
</tr>
<tr>
<th>3</th>
<td align="right" >31</td>
<td align="right" >32</td>
<td align="right" >33</td>
<td align="right" >34</td>
<td align="right" >35</td>
<td align="right" >36</td>
<td align="right" >37</td>
<td align="right" >38</td>
<td align="right" >39</td>
<td align="right" >40</td>
</tr>
<tr>
<th>2</th>
<td align="right" >21</td>
<td align="right" >22</td>
<td align="right" >23</td>
<td align="right" >24</td>
<td align="right" >25</td>
<td align="right" >26</td>
<td align="right" >27</td>
<td align="right" >28</td>
<td align="right" >29</td>
<td align="right" >30</td>
</tr>
<tr>
<th>1</th>
<td align="right" >11</td>
<td align="right" >12</td>
<td align="right" >13</td>
<td align="right" >14</td>
<td align="right" >15</td>
<td align="right" >16</td>
<td align="right" >17</td>
<td align="right" >18</td>
<td align="right" >19</td>
<td align="right" >20</td>
</tr>
<tr>
<th>0</th>
<td align="right" >1</td>
<td align="right" >2</td>
<td align="right" >3</td>
<td align="right" >4</td>
<td align="right" >5</td>
<td align="right" >6</td>
<td align="right" >7</td>
<td align="right" >8</td>
<td align="right" >9</td>
<td align="right" >10</td>
</tr>
<tr>
<th>Y/X</th>
<th>0</th>
<th>1</th>
<th>2</th>
<th>3</th>
<th>4</th>
<th>5</th>
<th>6</th>
<th>7</th>
<th>8</th>
<th>9</th>
</tbody>
</table>

### Move explain

Options
```
--xPos(-x) x first position
--yPos(-y) y first position
--facing(-f) where the rover is looking first time
--height(-H) height of the grid
--width(-w) width of the grid
--obstacles(-0) number obstecles to generate
obstacles-position(-O) cell location of obstacles given by the grid table must match the number of obstecles and can't be repeted nunber 

Commands: String capitalize of instructions to execute, rover movement F -> move fordward cell, L -> move to left looking cell, R -> move to right looking cell
```


Execute
```bash
bin/console app:rover --facing=N --xPos=0 --yPos=0 --height=10 --width=10 --obstacles=6 -O 13 -O 39 -O 32 -O 64 -O 48 -O 99 FRLRFRLLFFL
```

will reponse 

```
Rover on 0 X, 0 Y facing N
Rover moved to 0 X, 1 Y facing N
Rover moved to 1 X, 1 Y facing E
Rover moved to 1 X, 2 Y facing N
Rover moved to 2 X, 2 Y facing E
Rover moved to 3 X, 2 Y facing E
Rover moved to 3 X, 1 Y facing S
Rover moved to 4 X, 1 Y facing E
Rover moved to 4 X, 2 Y facing N
Rover moved to 4 X, 3 Y facing N
Rover moved to 4 X, 4 Y facing N
Rover moved to 3 X, 4 Y facing W
```
Executing 
```bash
bin/console app:rover --facing=N --xPos=0 --yPos=0 --height=10 --width=10 --obstacles=6 -O 13 -O 39 -O 35 -O 64 -O 48 -O 99 FRLRFRLLFFL
```
will response

```
Rover on 0 X, 0 Y facing N
Rover moved to 0 X, 1 Y facing N
Rover moved to 1 X, 1 Y facing E
Rover moved to 1 X, 2 Y facing N
Rover moved to 2 X, 2 Y facing E
Rover moved to 3 X, 2 Y facing E
Rover moved to 3 X, 1 Y facing S
Rover moved to 4 X, 1 Y facing E
Rover moved to 4 X, 2 Y facing N

                                                                                                                        
 [ERROR] Error: Obstacle detected moving Forward on 4 X, 3 Y moving back to 4 X, 2 Y facing N                           
                                                                                                                        
```

the error is because obstacle on cell 35 correspondent to 4X3Y
